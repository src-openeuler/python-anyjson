%global _empty_manifest_terminate_build 0
Name:		python-anyjson
Version:	0.4.0
Release:	1
Summary:	Wraps the best available JSON implementation available in a common interface
License:	BSD-3-Clause
URL:		https://github.com/catalyst/anyjson
Source0:	https://github.com/catalyst/anyjson/archive/refs/tags/0.4.0.tar.gz
Patch01:        fix-py3k.patch
BuildArch:	noarch


%description
Anyjson loads whichever is the fastest JSON module installed and provides
a uniform API regardless of which JSON implementation is used.

%package -n python3-anyjson
Summary:	Wraps the best available JSON implementation available in a common interface
Provides:	python-anyjson
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-anyjson
Anyjson loads whichever is the fastest JSON module installed and provides
a uniform API regardless of which JSON implementation is used.

%package help
Summary:	Development documents and examples for anyjson
Provides:	python3-anyjson-doc
%description help
Anyjson loads whichever is the fastest JSON module installed and provides
a uniform API regardless of which JSON implementation is used.

%prep
%autosetup -n anyjson-%{version} -p1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-anyjson -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Apr 10 2023 Ge Wang <wang__ge@126.com> - 0.4.0-1
- Update to version 0.4.0

* Tue May 10 2022 wulei <wulei80@h-partners.com> - 0.3.3-3
- License compliance rectification

* Wed Feb 09 2022 wulei <wulei80@huawei.com>
- Fix in anyjson setup command use_2to3 is invalid

* Mon Jul 06 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
